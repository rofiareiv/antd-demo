import { Layout, Menu, Icon } from 'antd';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
// import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';

import Home from './components/Home';
import Crm from './components/Crm';

const { Header, Sider, Content } = Layout;
// const SubMenu = Menu.SubMenu;
// const MenuItemGroup = Menu.ItemGroup;


class App extends Component {

  state = {
    collapsed: false,
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    })
  }

  // handleClick = (e) => {
  //   console.log('click', e);
  // }

  render() {
    return (
      <Router>
        <Layout>
          <Sider
            trigger={null}
            collapsible
            collapsed={this.state.collapsed}
          >
            <div className="logo"></div>
            <Router>
              <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} defaultOpenKeys={['sub1']}>
                <Menu.Item key="1">
                  <Icon type="home" />
                  <span><Link className="link" to="/">Home</Link></span>
                </Menu.Item>
                <Menu.Item key="4">
                  <Icon type="user" />
                  <span><Link className="link" to="/crm">CRM</Link></span>
                </Menu.Item>
                <Menu.Item key="5">
                  <Icon type="video-camera" />
                  <span>nav 2</span>
                </Menu.Item>
                <Menu.Item key="6">
                  <Icon type="upload" />
                  <span>nav 3</span>
                </Menu.Item>
              </Menu>
            </Router>
          </Sider>
          <Layout>
            <Header style={{ background: '#fff', padding: 0 }}>
              <Icon
                className="trigger"
                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={this.toggle}
              />
            </Header>
            <Content style={{
              margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280,
            }}
            >
              Content
              <Route path="/" exact component={Home} />
              <Route path="/crm" component={Crm} />
            </Content>
          </Layout>
        </Layout>
      </Router>

      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <p>
      //       Edit <code>src/App.js</code> and save to reload.
      //     </p>
      //     <a
      //       className="App-link"
      //       href="https://reactjs.org"
      //       target="_blank"
      //       rel="noopener noreferrer"
      //     >
      //       Learn React
      //     </a>
      //     <Button type="primary">Button Antd</Button>
      //   </header>
      // </div>      
    );
  }
}

export default App;
